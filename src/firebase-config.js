import firebase from 'firebase/app'
import 'firebase/firestore'
// import 'firebase/auth'
import 'firebase/database'
require('firebase/auth')

var firebaseConfig = {
  apiKey: "AIzaSyAlu61slKS1ryFI5T8VFlurh32S2vGPj5s",
  authDomain: "ullo-9c5aa.firebaseapp.com",
  databaseURL: "https://ullo-9c5aa-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "ullo-9c5aa",
  storageBucket: "ullo-9c5aa.appspot.com",
  messagingSenderId: "50061477604",
  appId: "1:50061477604:web:68e968e888377520474ef1",
  measurementId: "G-W67SRCJTYS"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

export default firebaseApp.firestore();

// import firebase from "firebase/app";
// import "firebase/database";
// import 'firebase/auth'

// let config = {
//   apiKey: "AIzaSyAItGdXBrXo7op3BfjfN5wmIwTfEnO1ZeY",
//   authDomain: "testbd-2744d.firebaseapp.com",
//   projectId: "testbd-2744d",
//   storageBucket: "testbd-2744d.appspot.com",
//   messagingSenderId: "950044112628",
//   appId: "1:950044112628:web:357523a29e9485cbcdd0a1"
// };

// firebase.initializeApp(config);

// export default firebase.database();