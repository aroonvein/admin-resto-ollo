import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/login/Login.vue'
import Home from '../views/Home.vue'
import Create from '../views/create/Create.vue'
import UpdateCafe from '../views/update/UpdateCafe.vue'
import PageNotFound from '../components/404/PageNotFound.vue'

import firebase from 'firebase/app'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'Авторизация',
    meta: {layout: 'empty'},
    component: () => import('../views/login/Login.vue')
  },
  {
    path: '/',
    name: 'Главная',
    meta: {layout: 'main', auth: true},
    component: () => import('../views/Home.vue'),
  },
  {
    path: '/create',
    name: 'Добавление нового заведения',
    meta: {layout: 'main', auth: true},
    component: () => import('../views/create/Create.vue'),
  },
  {
    path: '/cafes/:id',
    name: 'Редактирование заведения',
    meta: {layout: 'main', auth: true},
    component: () => import('../views/update/UpdateCafe.vue'),
  },
  { path: "*", component: PageNotFound }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  document.title = `Ollo - ${ to.name }`

  const currentUser = firebase.auth().currentUser
  const requireAuth = to.matched.some(record => record.meta.auth)

  if (requireAuth && !currentUser) {
    next('/login')
  } else {
    next()
  }
})

export default router
