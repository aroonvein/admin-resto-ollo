import Vue from 'vue'
import 'normalize.css/normalize.css' // a modern alternative to CSS resets

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import locale from 'element-ui/lib/locale/lang/ru-RU'

import '@/styles/index.scss'
import App from './App.vue'
import router from './router'
import store from './store'

import VueCookies from 'vue-cookies'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'
import 'firebase/storage'

firebase.initializeApp = ({
  apiKey: "AIzaSyAlu61slKS1ryFI5T8VFlurh32S2vGPj5s",
  authDomain: "ullo-9c5aa.firebaseapp.com",
  databaseURL: "https://ullo-9c5aa-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "ullo-9c5aa",
  storageBucket: "ullo-9c5aa.appspot.com",
  messagingSenderId: "50061477604",
  appId: "1:50061477604:web:68e968e888377520474ef1",
  measurementId: "G-W67SRCJTYS"
})

Vue.config.productionTip = false
Vue.use(ElementUI, { locale })
Vue.use(VueCookies)

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    store.dispatch('cafes/getListCafes')
      .then(()=> {
        app = new Vue({
          router,
          store,
          render: h => h(App)
        }).$mount('#app')
      })
  } else {
    console.log('Loading');
  }
})

