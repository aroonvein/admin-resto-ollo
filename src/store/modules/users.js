import db from '../../firebase-config'

const state = {
  usersList: []
}

const mutations = {
  setUsersList (state, payload) {
    state.usersList = payload
  }
}

const actions = {
  getListUsers({commit}) {
    db.collection('users')
    .get()
    .then(querySnapshot => {
      const documents = querySnapshot.docs.map(doc => doc.data())
      console.log("setUsersList", documents);
      commit('setUsersList', documents)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}