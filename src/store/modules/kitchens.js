import db from '../../firebase-config'

const state = {
  kitchens: []
}

const mutations = {
  retrieveKitchens (state, kitchens) {
    state.kitchens = kitchens
  },
  addKitchen(state, kitchen) {
    state.kitchens.push({
      id: kitchen.id,
      name: kitchen.name
    })
  },
  updateKitchen(state, kitchen) {
    const index = state.kitchens.findIndex(item => item.id == kitchen.id)
    state.kitchens.splice(index, 1, {
      'id': kitchen.id,
      'name': kitchen.name
    })
  },
  deleteKitchen(state, id) {
    const index = state.kitchens.findIndex(item => item.id == id)
    state.kitchens.splice(index, 1)
  },
}

const actions = {
  retrieveKitchens(context) {
    db.collection('kitchens').get()
    .then(querySnapshot => {
      let tempKitchens = []
      querySnapshot.forEach(doc => {
        const data = {
          id: doc.id,
          name: doc.data().name,
        }
        tempKitchens.push(data)
      })
      context.commit('retrieveKitchens', tempKitchens)
    })
  },

  updateKitchen(context, kitchen) {
    db.collection('kitchens').doc(kitchen.id).set({
      id: kitchen.id,
      name: kitchen.name
    })
      .then(() => {
        context.commit('updateKitchen', kitchen)
      })
  },

  deleteKitchen(context, id) {
    db.collection('kitchens').doc(id).delete()
      .then(() => {
        context.commit('deleteKitchen', id)
      })
  },

  addKitchen(context, kitchen) {
    db.collection('kitchens').add({
      name: kitchen.name
    })
    .then(docRed => {
      context.commit('addKitchen', {
        id: docRed.id,
        name: kitchen.name
      })
    })
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}