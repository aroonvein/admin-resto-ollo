import db from '../../firebase-config'

const state = {
  staffList: []
}

const mutations = {
  setStaffList (state, payload) {
    state.staffList = payload
  }
}

const actions = {
  getListstaff({commit}) {
    db.collection('staff')
    .get()
    .then(querySnapshot => {
      const documents = querySnapshot.docs.map(doc => doc.data())
      console.log("setStaffList", documents);
      commit('setStaffList', documents)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}