import firebase from 'firebase/app'

const state = {
  user: {}
}

const mutations = {
  setUser(state, user) {
    state.user = user
  },
  clearInfo(state) {
    state.user = {}
  },
}

const actions = {
  async login({dispatch, commit}) {
    let provider = new firebase.auth.GoogleAuthProvider();
      firebase
        .auth()
        .signInWithPopup(provider)
        .then((result) => {
          commit('setUser', firebase.auth().currentUser.email)
          // let token = result.credential.accessToken;
          // let user = result.user;
          //   console.log(token)
          //   console.log(user)
        })
        .catch((err) => {
          console.log(err);
        });
  },
  infoUser({ commit }) {
    commit('setUser', firebase.auth().currentUser.email)
  },
  async logout({commit}) {
    await firebase.auth().signOut()
    commit('clearInfo')
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}