import db from '../../firebase-config'

const state = {
  cafesList: [],
  cafelistFilter: []
}

const mutations = {
  setCafesList (state, payload) {
    // console.log('setCafesList');
    state.cafesList = payload
    state.cafelistFilter = payload
  },
  setCafelistFilter (state, payload) {
    state.cafelistFilter = payload
  },
  updateOneCafe(state, payload) {
    const index = state.cafesList.findIndex(item => item.id == payload.id)
    state.cafesList.splice(index, 1, payload)   
  },
  addCafesList(state, cafe) {
    state.cafesList.push(cafe)
  },
  addCafesListFilter(state, cafe) {
    state.cafelistFilter.push(cafe)
  },
  deleteCafe(state, id) {
    const index = state.cafesList.findIndex(item => item.id == id)
    state.cafesList.splice(index, 1)
    const index2 = state.cafelistFilter.findIndex(item => item.id == id)
    state.cafelistFilter.splice(index2, 1)
  },
}

const actions = {
  getListCafes({commit}) {
    db.collection('cafes').get()
    .then(querySnapshot => {
      const documents = querySnapshot.docs.map(doc => doc.data())
      console.log("VUEX CAFE LIST", documents);
      commit('setCafesList', documents)
      return documents
    })
    .catch((error) => {
      console.log("VUEX CAFE LIST ERROR", error);
    })
  },

  filerListCafes({commit}, data ) {
    const lowerCasedStatus = data.status;
    const lowerCasedTitle = data.title.toLowerCase();
    
    if (lowerCasedStatus !== '' && lowerCasedTitle !== '') {
      // console.log('cтатус и имя кафе заполенны');
      const arrayDataFilterStatus = state.cafesList.filter((item) => item.status === lowerCasedStatus)
      const arryDataFilterTitle = arrayDataFilterStatus.filter((item) => item.name.toLowerCase().includes(lowerCasedTitle.toLowerCase()))
      return commit('setCafelistFilter', arryDataFilterTitle)
    }
    if (lowerCasedStatus !== '') {
      // console.log('Только заполнен фильтр статуса');
      const sdvsdv = state.cafesList.filter((item) => item.status === lowerCasedStatus)
      return commit('setCafelistFilter', sdvsdv)
    }
    if (lowerCasedTitle !== '') {
      const sdvasdvasd = state.cafesList.filter((item) => item.name.toLowerCase().includes(lowerCasedTitle.toLowerCase()))
      return commit('setCafelistFilter', sdvasdvasd)
    }
    if (lowerCasedTitle === '' && lowerCasedStatus === '') {
      return commit('setCafelistFilter', state.cafesList)
    }
  },

  createCafe({commit}, cafe) {
    console.log('VUEX CREATE CAFE DATA - ', cafe);
    const cafeDocument = db.collection('cafes').doc()
    db.collection('cafes').doc(cafeDocument.id).set(Object.assign({}, {
      id: cafeDocument.id,
      address: cafe.address,
      breaks: cafe.breaks,
      // dinners: cafe.dinners,
      firstApply: cafe.firstApply,
      geo: cafe.geo,
      kitchens: cafe.kitchens,
      logo: cafe.logo,
      lunches: cafe.lunches,
      name: cafe.name,
      phone: cafe.phone,
      site: cafe.site,
      status: cafe.status
    }))
      .then(() => {
        cafeDocument.get()
          .then(doc => {
            const cafe = doc.data()
            db.collection('staff').doc(cafe.id).set(Object.assign({}, {
              applied: [],
              approved: [],
            }))
            .then(() => {
              console.log('DONE CREATE SATFF');
              // commit('addCafesList', cafe)
              commit('addCafesListFilter', cafe)
            })
            .catch((error) => {
              console.log('ERROR CREATE STAFF', error);
            })
          })
      })
      .catch((error) => {
        console.log('ERROR CREATE CAFE', error);
      })
  },

  saveUpdateCafe({commit}, cafe) {
    console.log("333 Cafe" + cafe)
    db.collection('cafes').doc(cafe.id).update(Object.assign({}, {
      id: cafe.id,
      address: cafe.address,
      breaks: cafe.breaks,
      // dinners: cafe.dinners,
      firstApply: cafe.firstApply,
      geo: cafe.geo,
      kitchens: cafe.kitchens,
      logo: cafe.logo,
      lunches: cafe.lunches,
      name: cafe.name,
      phone: cafe.phone,
      site: cafe.site,
      status: cafe.status
    }))
    .then(() => {
      commit('updateOneCafe', cafe)
      console.log('DONE UPDATE CAFE');
    })
    .catch((error) => {
      console.log('ERROR UPDATE CAFE', error);
    })
  },

  updateStatusCafe({commit}, cafe) {
    db.collection('cafes').doc(cafe.id).set(cafe)
    .then(() => {
      commit('updateOneCafe', cafe)
      console.log('DONE UPDATE STATUS CAFE');
    })
  },
  deleteCafe({commit}, id) {
    db.collection('cafes').doc(id).delete()
      .then(() => {
        commit('deleteCafe', id)
      })
  },
  
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}