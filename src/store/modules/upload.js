const state = {
  urlImageMenu: null,
  urlImageAvatar: null
}

const mutations = {
  setUrlImageMenu (state, url) {
    state.urlImageMenu = url
  },
  setUrlImageAvatar (state, url) {
    state.urlImageAvatar = url
  }
}

const actions = {
  createUrlImageMenu(context, file) {
    context.commit('setUrlImageMenu', file) 
  },
  createUrlImageAvatar(context, file) {
    context.commit('setUrlImageAvatar', file) 
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}