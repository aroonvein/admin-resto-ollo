import Vue from 'vue'
import Vuex from 'vuex'

import auth from './modules/auth'
import cafes from './modules/cafes'
import kitchens from './modules/kitchens'
import staff from './modules/staff'
import upload from './modules/upload'
import users from './modules/users'


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    auth,
    cafes,
    kitchens,
    staff,
    upload,
    users
  }
})
